import { Component, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, ValidatorFn, AbstractControl  } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { destinoViaje } from '../models/destino-viaje.model';
import { MyValidation } from '../validaciones/my-Validacion';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax,} from 'rxjs/ajax';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<destinoViaje>;
  fg: FormGroup;
  minLongitud!: 6;
  searchResult!: string[];

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
              Validators.required,
              MyValidation.validNameParametrizable(15)
            ])],
      url:['', Validators.required] 
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Cambio de formularios: ', form);
    });
   }

  ngOnInit() {

    const searchBox = document.getElementById('nombre') as HTMLInputElement;
    const typeahead = fromEvent(searchBox, 'input').pipe(
      map(e => (e.target as HTMLInputElement).value), //Casi no encuentro como arreglar esta linea
      filter(text => text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(() => ajax('/assets/datos.json'))
        ).subscribe(ajaxResponse => {
          this.searchResult = ajaxResponse.response;
    })

    
  }

  guardar(nombre: string, url: string): boolean {
    const d = new destinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

}
