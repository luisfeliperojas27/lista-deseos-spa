import { destinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject, PartialObserver } from 'rxjs';

export class DestinosApiClient {
  /*elegir(e: destinoViaje) {
    throw new Error('Method not implemented.');
  }*/
	destinos: destinoViaje[] = [];
	//current: Subject<destinoViaje> = new BehaviorSubject<destinoViaje>();
	
	constructor() {
       this.destinos = [];
	}
	
	add(d: destinoViaje) {
	  this.destinos.push(d);
	}
	
	getAll(): destinoViaje[] {
	  return this.destinos;
    }

	getById(id: string): destinoViaje {
		return this.destinos.filter(function(d) {return d.id.toString() === id;})[0];
	}

	elegir(d: destinoViaje) {
		this.destinos.forEach(x => x.setSelected(false));
		d.setSelected(true);
		//this.current.next(d);
	}

	subscribeOnChange(fn: PartialObserver<destinoViaje> | undefined) {
		//this.current.subscribe(fn);
	}

} 