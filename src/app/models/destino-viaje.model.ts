export class destinoViaje {
    /*voteDown() {
        throw new Error('Method not implemented.');
    }
    voteUp() {
        throw new Error('Method not implemented.');
    }*/
    private selected!: boolean;
    public servicios: string[];
	id: any;

    constructor(public nombre: string, public u: string) {
        this.servicios = ['Desayuno', 'Almuerzo', 'Comida', 'Gym', 'Piscina', 'Spa'];
    }

    isSelected(): boolean {
        return this.selected;
    }
    setSelected(s: boolean) {
        this.selected = true;
    }
}