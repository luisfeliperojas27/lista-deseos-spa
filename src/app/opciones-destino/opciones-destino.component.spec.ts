import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpcionesDestinoComponent } from './opciones-destino.component';

describe('OpcionesDestinoComponent', () => {
  let component: OpcionesDestinoComponent;
  let fixture: ComponentFixture<OpcionesDestinoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpcionesDestinoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpcionesDestinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
